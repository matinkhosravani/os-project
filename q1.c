#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv) {

    int fd1[2];  // Used to store two ends of first pipe
    int fd2[2];  // Used to store two ends of second pipe

    char fixed_str[] = "The sum of even digits in the input number:";
    int p;

    if (pipe(fd1) == -1) {
        printf(1, "Pipe Failed");
        return 1;
    }
    if (pipe(fd2) == -1) {
        printf(1, "Pipe Failed");
        return 1;
    }

    p = fork();

    if (p < 0) {
        printf(1, "fork Failed");
        return 1;
    }

        // Parent process
    else if (p > 0) {
        char fixed_str[100];
        close(fd1[0]);  // Close reading end of first pipe
        // Write input string and close writing end of first
        // pipe.
        write(fd1[1], argv[1], strlen(argv[1]) + 1);
        close(fd1[1]);
        // Wait for child to send a string
        wait();
        close(fd2[1]); // Close writing end of second pipe
        // Read string from child, print it and close
        // reading end.
        read(fd2[0], fixed_str, 100);
        printf(1, "%s\n", fixed_str);
        close(fd2[0]);
        exit();
    }

        // child process
    else {
        close(fd1[1]);  // Close writing end of first pipe
        // Read a string using first pipe
        char concat_str[100];
        read(fd1[0], concat_str, 100);
        int sum = 0;
        for (int j = 0; j < strlen(concat_str); ++j) {
            int x = concat_str[j] - '0';
            if (x % 2 == 0) {
                sum += x;
            }
        }

        int m = 0;
        char ssum[10];

        /**
         * to change sum into character.
         */
        while (sum != 0) {
            char c = (sum % 10) + '0';
            ssum[m] = c;
            sum = sum / 10;
            m++;
        }


        // Concatenate a fixed string with it
        int k = strlen(fixed_str);
        int i;
        for (i = strlen(ssum); i > 0; i--) {
            fixed_str[k++] = ssum[i - 1];
        }

        fixed_str[k] = '\0';   // string ends with '\0'

        // Close both reading ends
        close(fd1[0]);
        close(fd2[0]);

        // Write concatenated string and close writing end
        write(fd2[1], fixed_str, strlen(fixed_str) + 1);
        close(fd2[1]);

        exit();
    }
}


